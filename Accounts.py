#!/usr/bin/env python

import random
import getpass
import TFA

accounts = {}
numbers = {}

def main():
    while True:
        print("\n\nSelect an option!\n1. Create an Account\n2. Login\n3. Change Password\n4. Change Number")
        ainput = input("Option: ")
        
        if (ainput == "1"):
            createme()
        if (ainput == "printall"):
            print(accounts)
            print(numbers)
        if (ainput == "2"):
            logmein()
        if (ainput == "3"):
            changepass()
        if (ainput == "4"):
            changenum()
        if (ainput == "0"):
            return
       
def createme():
   username = input("Username: ")
   if username in accounts:
       return
   password = input("Password: ")
   number = input("Cellphone #: ")

   accounts[username] = password
   numbers[username] = number
   return

def changepass():
   username = input("Existing Username: ")
   if username not in accounts:
         return
   password = input("New Password: ")

   accounts[username] = password
   return

def changenum():
    username = input("Existing Username: ")
    if username not in numbers:
        return
    password = input("Password: ")
    number = input("New number: ")
    
    numbers[username] = number
    return

def logmein():
    print("LOGIN SYSTEM : Leave username blank to exit to main menu.")
    while True:
        username = input("Username: ")
        if username is "":
            return
        password = getpass.getpass("Password: ")
        if username in accounts:
            if accounts[username] == password:
                break
        print("Invalid Username and Password. Contact an administrator to reset your password.")
        
    if numbers[username] is not "":
        TFAuth = TFA.check(numbers[username])
        if TFAuth is True:
            loggedin()
            return
        else:
            return

    else:
        print("Two factor authentication is disabled for this account. To activate, please add your number.")
        loggedin()
        return

def loggedin():
   print("You logged in. Congrats. You won a cookie.")


#declare main function
if __name__ == "__main__":
   main()

