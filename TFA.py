import requests
import random

USERNAME = "fr0sto1245@gmail.com"
API_KEY = "6f473d48207a8677e11c68429c6c431f8e5ed8f3"
SEND = "https://platform.tillmobile.com/api/send?username={}&api_key={}"

def check(number):
    code = int(10000*random.random())
    text = "Your verification code is: " + str(code)

    msg = {
            'phone' : number,
            'text' : text
    }

    response = requests.post(SEND.format(USERNAME,API_KEY),json=msg)

    response.raise_for_status()
    response.json()

    while True:
        codecheck = input("Verification code: ")
        if codecheck is "":
            print("Verification cancelled.")
            break
        elif codecheck == str(code):
            return True
        else:
            print("Verification code invalid. Please try again later.")

    return False
